**Ingrediënten**
- 1 ui
- 1 teentje knoflook
- 500 gr. rundergehakt
- italiaanse kruiden (basilicum, oregano, majoraan, peterselie, paprikapoeder) en peper
- 1 klein blikje tomatenpuree
- 1 blik gepelde tomaten in stukjes
- Groente
- 1 pak voorgekookte lasagnevellen, olijfolie
- mozzarella
- 50 gr. boter
- 50 gr. bloem
- 500 ml. koude melk

**Recept**

Voor de bolognesesaus de ui snipperen en in een grote koekenpan met wat olijfolie fruiten, halverwege de fijngesneden knoflook toevoegen. Gehakt rullen met de kruiden, daarna in het midden tomatenpuree verwarmen.

Gepelde tomaten en groente toevoegen. Zorgen dat de groeten gaar is en dan is de saus klaar.

Voor de bechamelsaus de boter smelten (niet bruin laten worden), bloem erdoor roeren en verwarmen (schrik niet; het wordt een soort klont, maar dat komt goed!). Daarna beetje bij beetje de koude melk met een garde erdoor roeren. Laten koken en blijven roeren tot je een dikke saus hebt. Op smaak brengen met wat kruiden of peper.

In een ingevette ovenschaal 1 laag lasagnevellen leggen. Vervolgens bedekken met 1/3 van de bechamelsaus, 1/3 van de bolognesesaus en 1 in plakjes gesneden bolletje mozzarella. Nu weer herhalen met een laag lasagnevellen, saus en kaas, vervolgens nog zo'n laag maar dan zonder kaas. Afdekken met aluminium-folie en in de oven schuiven, boven het midden. Na 30 min. de folie verwijderen en nog 15 min. in de oven laten. Als het "knettert" is het klaar.