## Voedsel informatie

**Guidelines**
1. Zoveel mogelijk volkoren
2. Geen toegevoegde suiker
3. Energie wil je niet drinken, maar eten, drink dus water
4. Neem als tussendoor een volkoren cracker of ongezouten nootjes, rest is slechter

**Biologisch**

Boeit niet, belangrijkste reden om biologisch te eten zou bijvoorbeeld geen bestrijdingsmiddelen kunnen zijn. Die worden net zo goed gebruikt, alleen dan van nature voorkomende. Uit onderzoek blijkt dat dit net zo schadelijk en in sommige gevallen schadelijker is dan kunstmatige bestrijdingsmiddelen.

**E-nummers**

E-nummers zijn getest en er is een dagelijkse inname hoeveelheid van vastgesteld. Er zijn synthetische  e-nummers en biologische e-nummers. Het merendeel zijn stoffen die ook van nature voorkomen.

Het probleem zit in het goedkeuringsproces en de maximale intake hoeveelheid. De personen die het goedkeuren werken over het algemeen ook in/voor de voedingsindustrie en de maximale intake wordt niet op gecontroleerd over producten heen.
Het is dus handig zo min mogelijk bewerkt voedsel te eten (hier zitten namelijk altijd e-nummers of met naam genoemde toevoegingen in)

Omstreden e-nummers (allemaal kleurstoffen)
- E102 of tartrazine
- E104 of chinolinegeel
- E110 of zonnegeel
- E122 of azorubine
- E124 of ponceau
- E129 of allurarood

Dan moet er op de verpakking staan:
'Deze kleurstoffen kunnen de activiteit of oplettendheid van kinderen nadelig beïnvloeden.

**Suiker**

Suiker is suiker. Het gaat om de hoeveelheid en niet om welk suikerproduct. Kunstmatige zoetstoffen zijn erger.
Honing is marginaal beter, dit is vooral te danken aan complexiteit en extra voedingstoffen. **Let op marginaal!**